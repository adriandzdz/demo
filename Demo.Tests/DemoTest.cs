﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Tests
{
	[TestFixture]
	public class DemoTest
	{
		[Test]
		public void FirstTest()
		{
			Assert.AreEqual(1, 1);
		}

		[Test]
		public void LastTest()
		{
			Assert.AreEqual(1, 1);
		}


		[Test]
		public void OtherTest()
		{
			Assert.AreEqual(1,1);
		}
	}
}
